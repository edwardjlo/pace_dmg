﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueInteraction : MonoBehaviour {

    [SerializeField]
    Dialogues npc;

    [SerializeField]
    Text dialogueText;
    [SerializeField]
    Text leftText;
    [SerializeField]
    Text rightText;
    [SerializeField]
    Text middleText;

    [SerializeField]
    GameObject backPanel;
    [SerializeField]
    GameObject nextTreeButton;

    [SerializeField] 
    GameObject exampleBag; 
    [SerializeField]
    GameObject exampleInfoBook; 
    [SerializeField]
    GameObject exampleOpenBook;
    [SerializeField]
    GameObject examination; 
    // [SerializeField]
    // GameObject itemName;
    [SerializeField]
    GameObject itemDialogueBox;
    [SerializeField]
    GameObject buttonForDialogueBox; 

    //ID stuff 
    [SerializeField]
    GameObject NpcID; 
    [SerializeField]
    GameObject expiredText; 
    [SerializeField]
    GameObject exampleLicence;

    public int [] items; 
    public string[] treeNames;
    private int currentTree = 0;  

    bool nextEnd = false;

    // Simple Dialogues //
    // This is a basic example of how you can use the dialogue system. //

    
	void Start() {
        npc.SetTree(treeNames[currentTree]); //This sets the current tree to be used. Resets to the first node when called.
        
        NpcID.SetActive(false);
        exampleLicence.SetActive(false); 
        exampleBag.SetActive(false); 
        exampleInfoBook.SetActive(false);
        exampleOpenBook.SetActive(false);
        examination.SetActive(false);
        itemDialogueBox.SetActive(false);
        // itemName.SetActive(false);

        Display(); 
	}

    public void Choice(int index)
    {
        if (index == 2 && npc.GetCurrentTree() == "Second Conversation") index = 1;
            if (npc.GetChoices().Length != 0) {

                    npc.NextChoice(npc.GetChoices()[index]); //We make a choice out of the available choices based on the passed index.
                    Display();                               //We actually call this function on the left and right button's onclick functions
            
            }

            else {

                Progress();

            }
    }

    public void TalkAgain()
    {
        currentTree++; 
        currentTree = currentTree%treeNames.Length; 
        Debug.Log("I'm dumb" + currentTree + " " + treeNames[currentTree]); 
        npc.SetTree(treeNames[currentTree]);
        nextEnd = false;
        Display();
    }

    public void expiryDateChecker() { 

        middleText.text = "Ask about expired licence";
        //Setting the appropriate buttons visability
        leftText.transform.parent.gameObject.SetActive(false);
        rightText.transform.parent.gameObject.SetActive(false);
        middleText.transform.parent.gameObject.SetActive(true);

    }

    public void itemManagerChecker(string a) {

        Debug.Log("Debug checking");

            currentTree = 0; 

            for(int i = 0; i < treeNames.Length; i++) { 

               if(string.Compare(a, treeNames[i]) != 0) {

                    TalkAgain();


               } else { 

                    break; 

               }
            }

            Debug.Log(treeNames);
            Debug.Log(a);
            middleText.text = "Ask about this random thing";
            //Setting the appropriate buttons visability
            leftText.transform.parent.gameObject.SetActive(false);
            rightText.transform.parent.gameObject.SetActive(false);
            middleText.transform.parent.gameObject.SetActive(false);
            // itemName.SetActive(true); 
            itemDialogueBox.SetActive(true);
    }

    public void Progress()
    {
        npc.Next(); //This function returns the number of choices it has, in my case I'm checking that in the Display() function though.
        Display();
    }

    public void Display()
    {
        if (nextEnd == true)
        {
            backPanel.SetActive(false);
            nextTreeButton.SetActive(true);
        }
        else
        {
            backPanel.SetActive(true);
            nextTreeButton.SetActive(false);
        }

        //Sets our text to the current text
        dialogueText.text = npc.GetCurrentDialogue();
        //Just debug log our triggers for example purposes
        if (npc.HasTrigger()) {

            Debug.Log("Triggered: " + npc.GetTrigger());

            Debug.Log(npc.GetTrigger().GetType());

            if (string.Compare(npc.GetTrigger(), "ID") == 0) {

                NpcID.SetActive(true);
                Debug.Log("Got em");

            }

            if(string.Compare(npc.GetTrigger(), "closeID") == 0) {

                NpcID.SetActive(false);
                expiredText.SetActive(false);
                Debug.Log("Ha got em again");
                TalkAgain(); 

            }

            if(string.Compare(npc.GetTrigger(), "EgPics1") == 0) { 

                exampleLicence.SetActive(true); 
                exampleBag.SetActive(true); 

            }

            if(string.Compare(npc.GetTrigger(), "EgPics2") == 0) { 

                exampleLicence.SetActive(false);
                exampleBag.SetActive(false); 
                exampleInfoBook.SetActive(true);
                exampleOpenBook.SetActive(true); 

            }

            if(string.Compare(npc.GetTrigger(), "OpenBag") == 0) {

                examination.SetActive(true); 
                buttonForDialogueBox.SetActive(false);

            }

            if(string.Compare(npc.GetTrigger(), "StartGame") == 0) { 

                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

            }

            if(string.Compare(npc.GetTrigger(), "ExamineItem") == 0) { 



            }

        }
        //This checks if there are any choices to be made
        if (npc.GetChoices().Length != 0)
        {
            //Setting the text's of the buttons to the choices text, in my case I know I'll always have a max of three choices for this example.
            leftText.text = npc.GetChoices()[0];
            middleText.text = npc.GetChoices()[1];
            //If we only have two choices, adjust accordingly
            if (npc.GetChoices().Length > 2)
                rightText.text = npc.GetChoices()[2];
            else
                rightText.text = npc.GetChoices()[1];
            //Setting the appropriate buttons visability
            leftText.transform.parent.gameObject.SetActive(true);
            rightText.transform.parent.gameObject.SetActive(true);
            if(npc.GetChoices().Length > 2)
                middleText.transform.parent.gameObject.SetActive(true);
            else
                middleText.transform.parent.gameObject.SetActive(false);
        }

        else if(npc.HasTrigger() && string.Compare(npc.GetTrigger(), "ID") == 0) { 

            leftText.transform.parent.gameObject.SetActive(false);
            rightText.transform.parent.gameObject.SetActive(false);
            middleText.transform.parent.gameObject.SetActive(false);

        }
        
        else if(npc.HasTrigger() && string.Compare(npc.GetTrigger(), "OpenBag") == 0) { 

            leftText.transform.parent.gameObject.SetActive(false);
            rightText.transform.parent.gameObject.SetActive(false);
            middleText.transform.parent.gameObject.SetActive(false);

        }

        else if(npc.HasTrigger() && string.Compare(npc.GetTrigger(), "ExamineItem1") == 0) { 

            leftText.transform.parent.gameObject.SetActive(false);
            rightText.transform.parent.gameObject.SetActive(false);
            middleText.transform.parent.gameObject.SetActive(false);

        }

        else
        {
            middleText.text = "Continue";
            //Setting the appropriate buttons visability
            leftText.transform.parent.gameObject.SetActive(false);
            rightText.transform.parent.gameObject.SetActive(false);
            middleText.transform.parent.gameObject.SetActive(true);
        }
        
        if (npc.End()) //If this is the last dialogue, set it so the next time we hit "Continue" it will hide the panel
            nextEnd = true;
    }
}
