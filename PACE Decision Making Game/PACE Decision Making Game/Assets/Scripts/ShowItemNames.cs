﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowItemNames : MonoBehaviour
{

	public GameObject itemName; 
	public GameObject otherItem1; 
	public GameObject otherItem2; 
	public GameObject otherItem3; 


    // Start is called before the first frame update
    void Start()
    {
        
    	itemName.SetActive(false);
    	otherItem1.SetActive(false);
    	otherItem2.SetActive(false);
    	otherItem3.SetActive(false);

    }

    public void showItemName() { 

    	itemName.SetActive(true);

    }

    public void closeOtherItems() { 

    	otherItem1.SetActive(false);
    	otherItem2.SetActive(false);
    	otherItem3.SetActive(false);

    }

}
