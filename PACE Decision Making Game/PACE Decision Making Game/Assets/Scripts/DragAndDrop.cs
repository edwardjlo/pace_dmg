﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
 
 public class DragAndDrop : MonoBehaviour, IDragHandler {
 
     public static Vector3 move;
     Vector3 initialpos;
     Vector3 distance;
     float speed=0.2f;
     
     void Start()

     {
        move = Vector3.zero;
     }
 
     #region IDragHandler implementation
 
     public void OnDrag (PointerEventData eventData)
     {
         
        distance = Input.mousePosition - initialpos;
        transform.position = initialpos + distance;
        Vector3 move1 = distance.normalized;
        move.x = move1.x * speed;
        move.z = move1.y * speed;
        
 
     }
 
     #endregion
 
}