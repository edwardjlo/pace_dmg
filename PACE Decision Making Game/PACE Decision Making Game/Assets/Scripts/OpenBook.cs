using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenBook : MonoBehaviour
{

	public GameObject Book;  
	public GameObject OpenBookButton;  
	public GameObject infoBook;
    public GameObject FirstPage1; 
    public GameObject FirstPage2; 
    public GameObject lastPage;
	public GameObject closeButton;
    public GameObject closeButton2; 

    // Start is called before the first frame update
    void Start()
    {
        
    	Book.SetActive(false);

    }

    void Update() { 

    }

    public void OpeningBook () { 

    	Book.SetActive(true); 
    	infoBook.SetActive(false);
        closeButton.SetActive(true); 
        FirstPage1.SetActive(true);
        FirstPage2.SetActive(true);

    }

    public void closeBook() { 

    	Book.SetActive(false); 
    	infoBook.SetActive(true); 

    }

    public void onLastPage() { 

            closeButton.SetActive(false); 
            closeButton2.SetActive(true);

    }

}
