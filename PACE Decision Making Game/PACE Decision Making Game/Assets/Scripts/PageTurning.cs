using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageTurning : MonoBehaviour
{

	public GameObject book; 
	public GameObject lastFirstPage; 
	public GameObject lastSecondPage;
	public GameObject nextFirstPage; 
	public GameObject nextSecondPage;
	public GameObject backButton; 
	public GameObject nextButton; 
    public GameObject closeButton; 

    // Start is called before the first frame update
    void Start()
    {

  //   	lastFirstPage.SetActive(true); 
		// lastSecondPage.SetActive(true); 

		nextFirstPage.SetActive(false);
		nextSecondPage.SetActive(false); 
        
    }

    public void turningNextPage() { 

		lastFirstPage.SetActive(false); 
		lastSecondPage.SetActive(false); 

		nextFirstPage.SetActive(true);
		nextSecondPage.SetActive(true); 

    }

    public void turningLastPage() { 

    	lastFirstPage.SetActive(true); 
    	lastSecondPage.SetActive(true);

    	nextFirstPage.SetActive(false);
    	nextSecondPage.SetActive(false);

        closeButton.SetActive(true);

    }

    public void closeBook() { 

    	book.SetActive(false); 
        Debug.Log("I'm closing");
    	

    }

}
