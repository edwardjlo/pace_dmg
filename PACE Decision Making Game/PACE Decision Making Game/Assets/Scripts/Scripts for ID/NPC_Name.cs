﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_Name : MonoBehaviour
{

	public GameObject NPCNameTag; 

    // Start is called before the first frame update
    void Start()
    {

    	NPCNameTag.SetActive(false);
        
    }

    public void getNPCNameTag() { 

    	NPCNameTag.SetActive(true);

    }

}
