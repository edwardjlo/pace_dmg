﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagController : MonoBehaviour
{

    public GameObject NpcPictureTag;
    public GameObject DL_HeaderTag; 
    public GameObject AddressTag;
    public GameObject LicenceNumberTag; 
    public GameObject NPCNameTag;

    // // Start is called before the first frame update
    // void Start()
    // {
    //     NPCNameTag.SetActive(false); 
    //     NpcPictureTag.SetActive(false);
    //     DL_HeaderTag.SetActive(false);
    //     AddressTag.SetActive(false);
    //     LicenceNumberTag.SetActive(false);
    // }


    public void closeTags() { 

        NPCNameTag.SetActive(false); 
        NpcPictureTag.SetActive(false);
        DL_HeaderTag.SetActive(false);
        AddressTag.SetActive(false);
        LicenceNumberTag.SetActive(false);

    }

}
