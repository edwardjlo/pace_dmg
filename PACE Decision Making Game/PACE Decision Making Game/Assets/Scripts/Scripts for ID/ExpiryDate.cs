﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpiryDate : MonoBehaviour
{

	public GameObject expiredLicence; 

	void Start() {

		expiredLicence.SetActive(false);

	}

	public void caughtExpired() { 

		expiredLicence.SetActive(true); 

	}

}
