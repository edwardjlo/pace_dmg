﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_Picture : MonoBehaviour
{

	public GameObject NPCPictureTag; 

    // Start is called before the first frame update
    void Start()
    {

    	NPCPictureTag.SetActive(false);
        
    }

    public void getNPCPicture() { 

    	NPCPictureTag.SetActive(true);

    }

}
