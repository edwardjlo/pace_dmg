﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DL_Header : MonoBehaviour
{

	public GameObject DL_HeaderTag; 

    // Start is called before the first frame update
    void Start()
    {

    	DL_HeaderTag.SetActive(false); 
        
    }

    public void getHeaderTag() { 

    	DL_HeaderTag.SetActive(true); 

    }

}
