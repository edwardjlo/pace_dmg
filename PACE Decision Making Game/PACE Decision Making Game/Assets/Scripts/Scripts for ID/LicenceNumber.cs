﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LicenceNumber : MonoBehaviour
{

	public GameObject LicenceNumTag; 

    // Start is called before the first frame update
    void Start()
    {

    	LicenceNumTag.SetActive(false); 
        
    }

    public void getLicenceNumTag() { 

    	LicenceNumTag.SetActive(true); 

    }

}
