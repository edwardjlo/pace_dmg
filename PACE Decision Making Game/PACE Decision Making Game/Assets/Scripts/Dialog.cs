using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
 
public class Dialog : MonoBehaviour
{
 
    public TextMeshProUGUI displayText;
    public string[] sentences;
    private int textIndex;
    public float typeSpeed;
 
    public GameObject continueButton;
    // public GameObject ConvoBranch; 
    // public GameObject lastElement;
 
    void Start() {
 		
        StartCoroutine(Type());
        //ConvoBranch.SetActive(false); 
 
    }
 
    void Update() {
 
        if(displayText.text == sentences[textIndex]) {
 
            continueButton.SetActive(true);
 
        }
 
    }
 
    IEnumerator Type() {
 
        foreach(char letter in sentences[textIndex].ToCharArray()) {
 
            displayText.text +=letter;
            yield return new WaitForSeconds(typeSpeed);
 
        }
 
    }
 
    public void continueSentence() {
 
        continueButton.SetActive(false);
 
        if(textIndex < sentences.Length - 1) {
 
            textIndex++;
            displayText.text = "";
            StartCoroutine(Type());
 
        } else {
 
            displayText.text = "";  
            continueButton.SetActive(false);
 
        }

        // if (textIndex == sentences.Length) { 

        // 	lastElement.SetActive; 

        // }
 
    }
 
 
}