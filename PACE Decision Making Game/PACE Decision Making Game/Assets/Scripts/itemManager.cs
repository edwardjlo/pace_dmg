﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemManager : MonoBehaviour
{

	public GameObject item;
	// public GameObject itemInfoMenu; 
	// public GameObject itemText;
	public GameObject itemNameText; 
	public GameObject ExamineButton;
	public GameObject closeButton;

	// public int[] numOfItems; 
    // Start is called before the first frame update

    void Start()
    {
        //item.SetActive(false);         
        // itemInfoMenu.SetActive(false); 
        // itemText.SetActive(false);
        closeButton.SetActive(false);
        itemNameText.SetActive(false);
        ExamineButton.SetActive(false);
        
    }

    public void openItemInfo() { 

        // itemText.SetActive(true);
    	// itemInfoMenu.SetActive(true); 
    	itemNameText.SetActive(true);
    	closeButton.SetActive(true);

    }

    public void ExaminationButton() { 

    	ExamineButton.SetActive(true); 

    }

    public void CloseText() {

    	ExamineButton.SetActive(false);
    	// itemText.SetActive(false);
    	closeButton.SetActive(false); 
    	// itemInfoMenu.SetActive(false);
    	itemNameText.SetActive(false);

    }

}
